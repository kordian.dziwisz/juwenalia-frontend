import Vue from 'vue'
import Vuex from 'vuex'
import config from '@/config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    config: config,
    events: undefined,
    sponsors: undefined,
    news: undefined,
  },
  getters: {
    eventsLastUpdate: state => {
      return state.events.reduce((acc = new Date(0), event) => {
        return acc >= event.updatedAt ? acc : event.updatedAt
      })
    },
    newsLastUpdate: state => {
      return state.news.reduce((acc = new Date(0), news) => {
        return acc >= news.updatedAt ? acc : news.updatedAt
      })
    },
  },
  mutations: {
    setEvents(state, payload) {
      const events = payload.map(event => {
        return {
          ...event,
          updatedAt: new Date(event.updatedAt),
          createdAt: new Date(event.createdAt),
          date: new Date(event.date),
          dateEnd: new Date(event.dateEnd),
        }
      })
      state.events = events
    },
    setSponsors(state, payload) {
      const sponsors = payload.map(sponsor => {
        return {
          ...sponsor,
          updatedAt: new Date(sponsor.updatedAt),
          createdAt: new Date(sponsor.createdAt),
        }
      })
      state.sponsors = sponsors
    },
    setNews(state, payload) {
      const news = payload.map(news => {
        return {
          ...news,
          updatedAt: new Date(news.updatedAt),
          createdAt: new Date(news.createdAt),
          date: new Date(news.date),
        }
      })
      state.news = news
    },
  },
})
