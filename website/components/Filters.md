# Filters

## Props

<!-- @vuese:Filters:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|filters|-|`Array`|`false`|-|

<!-- @vuese:Filters:props:end -->


## Events

<!-- @vuese:Filters:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|emitFilters|-|-|

<!-- @vuese:Filters:events:end -->


## Methods

<!-- @vuese:Filters:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|emitFilters|Used to emit filters to parent component and closing popup with filters|default value is assigned to filters|
|castFilters|Used to cast filters array to object|default value is assigned to filters|
|setDate|Used to set data filter; dirty is used to be able to undo filtering|string with date from buttons|

<!-- @vuese:Filters:methods:end -->


