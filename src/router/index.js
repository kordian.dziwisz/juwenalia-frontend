import Vue from 'vue'
import VueRouter from 'vue-router'
import vTimetable from '@v/Timetable'
import vTimetableDetails from '@v/TimetableDetails'
import vNews from '@v/News'
import vNewsDetails from '@v/NewsDetails'
import vMap from '@v/Map'
import vContact from '@v/Contact'
import vAbout from '@v/About'
import vTerms from '@v/Terms'
import vHome from '@v/Home'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'homepage',
    component: vHome,
  },
  {
    path: '/harmonogram',
    name: 'timetable',
    component: vTimetable,
  },
  {
    path: '/harmonogram/:id',
    name: 'event-details',
    component: vTimetableDetails,
  },
  {
    path: '/news',
    name: 'news',
    component: vNews,
  },
  {
    path: '/news/:id',
    name: 'news-details',
    component: vNewsDetails,
  },
  {
    path: '/mapa',
    name: 'map',
    component: vMap,
  },
  {
    path: '/kontakt',
    name: 'contact',
    component: vContact,
  },
  {
    path: '/o_nas',
    name: 'about',
    component: vAbout,
  },
  {
    path: '/regulamin',
    name: 'terms',
    component: vTerms,
  },
  {
    path: '*',
    redirect: { name: 'homepage' },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
