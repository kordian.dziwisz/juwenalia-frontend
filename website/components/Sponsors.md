# Sponsors

## MixIns

<!-- @vuese:Sponsors:mixIns:start -->

| MixIn |
| ----- |
| Api   |

<!-- @vuese:Sponsors:mixIns:end -->

| Method      | Description                                  | Parameters |
| ----------- | -------------------------------------------- | ---------- |
| getSponsors | get sponsors from database or from firestore | -          |

<!-- @vuese:Sponsors:methods:end -->
