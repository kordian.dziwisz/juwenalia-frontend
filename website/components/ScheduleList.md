# ScheduleList

## Props

<!-- @vuese:ScheduleList:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|events|-|`Array`|`false`|-|

<!-- @vuese:ScheduleList:props:end -->


## Methods

<!-- @vuese:ScheduleList:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|goToDetails|router push to the details|{number} id of an event|

<!-- @vuese:ScheduleList:methods:end -->


