import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './sw/registerServiceWorker'
import vuetify from './plugins/vuetify'
import Vuelidate from 'vuelidate'
import vueMoment from 'vue-moment'
import moment from 'moment'
import 'moment/locale/pl'

Vue.config.productionTip = false
Vue.use(Vuelidate)

moment.locale('pl')
Vue.use(vueMoment, { moment })

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')
