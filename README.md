# juwenalia frontend

Projekt **[Słupskich juwenaliów](http://juwenalia.slupsk.pl/)**, single-page application **[Vue](https://vuejs.org/)**
tworzone z pomocą **[vue-cli](https://cli.vuejs.org/)** z docelową obsługą **[PWA](https://cli.vuejs.org/core-plugins/pwa.html)** i cache.

Aplikacja wykorzystuje API udostępnianie przez serwer https://gitlab.com/kordian.dziwisz/juwenalia-backend

Aby uruchomić serwer developerski podaj ip serwera w **[config.js](https://gitlab.com/kordian.dziwisz/juwenalia-frontend/-/blob/master/src/main.js)** i w konsoli w lokalizacji folderu z zawartością wpisz `npm run serve`

Aby zbudować aplikację użyj `npm run build`, aplikacja wyjściowa znajdzie się w folderze `/dist`, po tym zawartość można wstawić do serwera http
