# NewsDetails

## Methods

<!-- @vuese:NewsDetails:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|imgURL|Returns image URL from imgURL component|Property logo from object sponsor in sponsors array|

<!-- @vuese:NewsDetails:methods:end -->


## Computed

<!-- @vuese:NewsDetails:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|article|-|Returns article by id from VueX|No|

<!-- @vuese:NewsDetails:computed:end -->


## MixIns

<!-- @vuese:NewsDetails:mixIns:start -->
|MixIn|
|---|
|Api|

<!-- @vuese:NewsDetails:mixIns:end -->


