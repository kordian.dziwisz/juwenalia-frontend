export default class Event {
  /**
   *
   * @param {object} args {id, title, description, date, place}
   *
   */
  constructor(args) {
    try {
      if (typeof args.id === 'number') {
        this.id = args.id
      } else {
        throw 'Event constructor: typeof id !== "number"'
      }
      if (typeof args.title === 'string') {
        this.title = args.title
      } else {
        throw 'Event constructor: typeof title === "string"'
      }
      if (typeof args.description === 'string') {
        this.description = args.description
      } else {
        throw 'Event constructor: typeof description === "string"'
      }
      if (args.date instanceof Date) {
        this.date = args.date
      } else {
        throw 'Event constructor: args.date instanceof Date'
      }
      if (typeof args.place === 'string') {
        this.place = args.place
      } else {
        throw 'Event constructor: args.place === "string"'
      }
    } catch (e) {
      console.log(e)
    }
  }
}
