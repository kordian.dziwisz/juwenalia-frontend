# Timetable

## Methods

<!-- @vuese:Timetable:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getFilteredList|Get filtered list based on criteria|Filters object|

<!-- @vuese:Timetable:methods:end -->


## Computed

<!-- @vuese:Timetable:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|events|-|Returns events from VueX store if list is not filtered|No|

<!-- @vuese:Timetable:computed:end -->


## MixIns

<!-- @vuese:Timetable:mixIns:start -->
|MixIn|
|---|
|Api|

<!-- @vuese:Timetable:mixIns:end -->


