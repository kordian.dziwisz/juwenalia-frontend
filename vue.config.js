const path = require('path')
module.exports = {
  transpileDependencies: ['vuetify'],
  configureWebpack: {
    entry: './src/main.js',
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        '@v': path.resolve(__dirname, 'src/views'),
        '@c': path.resolve(__dirname, 'src/components'),
      },
    },
  },
}
