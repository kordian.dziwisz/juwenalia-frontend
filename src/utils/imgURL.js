/**
 *
 * @param {String} ip server ip
 * @param {String} path path to image (except /image/)
 */
export default function(ip, path, port = 80) {
  return `http://${ip}:${port}/image?path=${path}`
}
