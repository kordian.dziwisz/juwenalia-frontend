# TimetableDetails

## Computed

<!-- @vuese:TimetableDetails:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|event|-|Returns event by id from VueX|No|

<!-- @vuese:TimetableDetails:computed:end -->


## MixIns

<!-- @vuese:TimetableDetails:mixIns:start -->
|MixIn|
|---|
|Api|

<!-- @vuese:TimetableDetails:mixIns:end -->


