# News

## Methods

<!-- @vuese:News:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|imgURL|Returns image URL from imgURL component|Property logo from object sponsor in sponsors array|
|goToDetails|Redirects to details of news|Article id from article object in articles array|

<!-- @vuese:News:methods:end -->


## MixIns

<!-- @vuese:News:mixIns:start -->
|MixIn|
|---|
|Api|

<!-- @vuese:News:mixIns:end -->


