import validate from 'validate.js'
import constrains from '@/validations/api'
import config from '@/config'

/**
 * @param { String } ip
 * @param { "events" | "news" | "sponsors" } path
 * @param { Number } id
 * @param { Number } port
 */
function _request(path, id) {
  return new Request(
    validate({ id: id }, constrains).id === undefined
      ? `http://${config.server.ip}:${config.server.port}/${path}/${id}`
      : `http://${config.server.ip}:${config.server.port}/${path}`
  )
}

/**
 * @param {"events" | "news" | "sponsors"} path
 * @param {"GET" | "HEAD"} method
 */
function _fetch(path, id) {
  return fetch(_request(path, id), { method: 'GET', mode: 'cors' })
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

/**
 *
 * @param {"events" | "news" | "sponsors" } path
 * @param {Date} updatedAt
 * @param {Number} id
 */
async function _wasUpdated(path, updatedAt, id) {
  return new Date(await _fetch(`${path}/u`, 'GET', id)).getTime() === updatedAt.getTime()
}

/**
 * cast path to camelCase with set on the beginning.
 * @param { String } path
 * @returns
 */
function _setData(path) {
  return `set${path[0].toUpperCase()}${path.substring(1)}`
}

export default {
  methods: {
    /**
     * fetch data from the server or vuex
     * @param {"events" | "news" | "sponsors" } path
     * @param {Date} updatedAt
     * @param {Number} id
     * @returns {Promise<Object> | Object} fetched data
     */
    async getData(path, updatedAt, id) {
      if (this.$store.state[path] !== undefined) {
        if (updatedAt !== undefined && (await _wasUpdated(path, updatedAt, id))) {
          return this.$store.state[path]
        } else {
          return _fetch(path, 'GET', id).then(data => {
            this.$store.commit(_setData(path), data)
            return data
          })
        }
      } else {
        return _fetch(path, 'GET', id).then(data => {
          this.$store.commit(_setData(path), data)
          return data
        })
      }
    },

    getEvents(updatedAt, id) {
      return this.getData('events', updatedAt, id)
    },

    getNews(updatedAt, id) {
      return this.getData('news', updatedAt, id)
    },

    getSponsors(updatedAt, id) {
      return this.getData('sponsors', updatedAt, id)
    },
  },
}
