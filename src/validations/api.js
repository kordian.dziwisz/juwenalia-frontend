export default {
  address: {
    type: 'string',
    presence: true,
  },
  port: {
    type: 'integer',
    presence: true,
    numericality: {
      onlyInteger: true,
      greaterThanOrEqualTo: 0,
      strict: true,
    },
  },
  id: {
    presence: true,
    type: 'integer',
    numericality: {
      onlyInteger: true,
      greaterThanOrEqualTo: 0,
      strict: true,
    },
  },
  updatedAt: {
    presence: true,
    type: 'date',
  },
}
