# AboutSponsors

## Methods

<!-- @vuese:AboutSponsors:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|imgURL|Returns image URL from imgURL component|Property logo from object sponsor in sponsors array|

<!-- @vuese:AboutSponsors:methods:end -->


## Computed

<!-- @vuese:AboutSponsors:computed:start -->
|Computed|Type|Description|From Store|
|---|---|---|---|
|sponsors|-|Returns sponsors from VueX store|Yes|

<!-- @vuese:AboutSponsors:computed:end -->


## MixIns

<!-- @vuese:AboutSponsors:mixIns:start -->
|MixIn|
|---|
|Api|

<!-- @vuese:AboutSponsors:mixIns:end -->


